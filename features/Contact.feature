Feature: Contact

  Scenario: Contact form submits correctly
    Given I am on "/contact"
    When I fill in "Your name" with "Behatter"
    And I fill in "Your email address" with "behat@example.com"
    And I fill in "Subject" with "Just a test"
    And I fill in "Message" with "I would like to test your contact form. Thank you!"
    And I press "Send message"
    Then I should see "Your message has been sent."
    And I should be on "/"

  Scenario: Contact form checks for proper email address - submitted form will stay on /contact
    Given I am on "/contact"
    When I fill in "Your name" with "Behatter"
    And I fill in "mail" with "bad email address"
    And I fill in "Subject" with "Just a test"
    And I fill in "Message" with "I would like to test your contact form. Thank you!"
    And I press "Send message"
    Then I should be on "/contact"
