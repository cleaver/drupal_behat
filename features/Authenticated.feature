Feature: Authenticated users

  @api
  Scenario: Administrator accounts should have access to add users
    Given I am logged in as a user with the "Administrator" role
    And I am on "/"
    When I click "People"
    Then I should see the link "Add user"
