Feature: Homepage

  Scenario: Main home page content and links are available
    Given I am on "/"
    Then I should see the link "Log in"
    And I should see the link "Contact"
